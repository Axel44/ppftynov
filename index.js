#! /usr/bin/env node

const { program } = require('commander');

//const list = require('./commands/list')
const new_project = require('./commands/new_project')
const add_filter = require('./commands/add_filter')
const del_filter = require('./commands/del_filter')
const add_step = require('./commands/add_step')
const del_step = require('./commands/del_step')

program.command('new').description('List all the TODO tasks').action(new_project)
program.command('add_filter <Name_new_Filters>').description('').action(add_filter)
program.command('del_filter <Name_del_Filters>').description('').action(del_filter)
program.command('add_step <id> <name> <nextid>').description('').action(add_step)
program.command('del_step <del_id>').description('').action(del_step)

program.parse()