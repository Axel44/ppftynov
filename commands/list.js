const conf = new (require('conf'))()
const chalk = require('chalk')

function list() {
    const todoList = conf.get('todo-list')

    if(todoList && todoList.lengh)
    {
        console.log(chalk.blue.bold('Les tâches en vert sont terminées. Les tâches en jaune ne sont toujours pas terminées.'))
    }
    else
    {
        console.log(chalk.red.bold('Vous n\'avait pas encore de tache'))
    }

    todoList.forEach((task, index) => {
        if(task.done) {
            console.log(
                chalk.greenBright('&{index}. ${task.text}')
            )
        }else
        {
            console.log(
                chalk.yellowBright('${index}. ${task.text}')
            )
        }
        
    });

}

module.exports = list