const chalk = require('chalk')
const fs = require('fs');

function add_step(id,name,nextid) {

    if(fs.existsSync('./config-filters.json')){

        let config_filters = require('../config-filters.json');
        config_filters.steps[id] = {filter: name ,params:[],next:nextid}
        console.log(config_filters)
        fs.writeFile('./config-filters.json', JSON.stringify(config_filters), function (err) {
            if (err) throw err;
            console.log('Updated!');
          });
    }
    else
    {
        console.log(chalk.red.bold('Le fichier config-filter.json n\'esxiste pas '))
    }

}

module.exports = add_step