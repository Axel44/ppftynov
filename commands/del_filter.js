const chalk = require('chalk')
const fs = require('fs');

function del_filter(nameFilter) {

    var filterInConfig = false
    if(fs.existsSync('./config-filters.json')){

        let config_filters = require('../config-filters.json');
        for (const [key, value] of Object.entries(config_filters.steps)) {
            if(value.filter == nameFilter)
            {
                filterInConfig = true
            }
        }

    }else
    {
        console.log('Le filter '+nameFilter+' n\'existe pas.')
        filterInConfig = true
    }

    if(!filterInConfig){
        fs.unlink('./filters/'+nameFilter+'.js', function (err) {
            if (err) throw err;
            console.log('File deleted!');
          });
    }
    else
    {
        console.log('Filters est toujours dans config-filters.json')
    }


}

module.exports = del_filter