# ppftYnov

## Commande

ppft new pour Initier un nouveau projet Créé l'arborescence projet avec un fichier de configuration vierge. Exemple : ppft new mon_super_projet

ppft add_filter Ajouter un filter Créé un nouveau filter dans un projet existant Exemple : ppft add_filter capitalize

ppft del_filter Supprimer un filter existant. Exemple : ppft del_filter capitalize 

ppft add_step Ajouter une step au fichier de configuration

    Cette commande prend les
    paramètres suivants :
    - id de step unique.
    - nom de filter
    - id step suivante 

Exemple : ppft add_step 10 capitalize 15

ppft del_step Supprimer une step du fichier de configuration

    Cette commande prend les
    paramètres suivants :
    - id de step unique.
    
Exemple : ppft del_step 10
